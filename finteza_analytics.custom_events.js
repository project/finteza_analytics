jQuery(document).ready(($) => {
  'use strict';

  $('[data-finteza-event]').bind('click', function () {
    const name = $(this).data('finteza-event');

    if (!!name && typeof fz === 'function') {
      fz('track', name);
    }

    return true;
  });
});
